#include <stdio.h>

int copy(char *source, char *destination, unsigned int lengthDestination)
{
    unsigned int i;
    unsigned int length;

    if(!source || !destination)
    {
      return -1;
    }
    
    // Compute source length
    length = 0;
    while(source[length] != 0)
    {
         length++;
    }


    // Check lenght
    if((length + 1) > lengthDestination)
    {
       return 0;
    }

    i=0;
    do
    {
       destination[i] = source[i];
    }while(source[i++] != 0);

    return i;
}

int main()
{



}
