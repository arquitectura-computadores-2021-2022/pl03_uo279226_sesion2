#include <stdio.h>
#include <time.h>


int main()
{
   struct timespec tStart, tEnd;
   double dElapsedTimeS


  // Empieza la medicion

  if(clock_gettime(CLOCK_REALTIME, &tStart)<0){
    return -1;
  }

  // Codigo
  int x=0;
  for(int i=0; i<100; i++){
   x=(x+i)*5;
  }



  // Finaliza la medicion

  if(clock_gettime(CLOCK_REALTIME, &tEnd)<0){
    return -1;
  }

  // Traduce a segundos

  dElapsedTimeS=(tEnd.tv_sec-tStart.tv_sec);
  dElapsedTimeS+=(tEnd.tv_nsec-tStart.tv_nsec)/1e+9;

  return 0;

}
